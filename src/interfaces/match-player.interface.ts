export interface IMatchPlayer {
    id: string;
    pieces: [number, number][];
}
