export interface IUser {
    id: string;
    name: string;
    email: string;
    password: string;
    wins: number;
    defeated: number;
    hoursPlayed: number;
    level: number;
    createdAt: Date;
    updatedAt: Date;
}
