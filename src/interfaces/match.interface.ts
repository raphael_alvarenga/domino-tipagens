import { IMatchPlayer } from "./match-player.interface";

export interface IMatch {
    id: string;
    status: string;
    players: IMatchPlayer[];
    match: [number, number][];
    createdAt: Date;
}
